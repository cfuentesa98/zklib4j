package cl.iodev.zklib4j;

public class Command {
    
    private final int USHRT_MAX = 65535;
    private final int headerCommand = 0x7D825050;
    private int len = 0;
    private int command = 0;
    private int checkSum = 0;
    private int sessionId = 0;
    private int replyId = 0;
    private byte[] arg;

    public Command(int command, int sessionId, int replyId, byte[] arg) {
        super();
        
        this.command = command;
        this.sessionId = sessionId;
        this.replyId = replyId;
        if (arg != null) this.arg = arg;
        
        this.len = 8;
        if (this.arg != null && this.arg.length > 0) {
            this.len = this.len + this.arg.length;
        }
        
        this.calculateCheckSum();
        
    }
    
    private void calculateCheckSum() {
        this.checkSum = 0;

        this.checkSum = this.checkSum + (this.command & 0x0000FFFF);
        if (this.checkSum > this.USHRT_MAX) {
            this.checkSum = this.checkSum - this.USHRT_MAX;
        }

        this.checkSum = this.checkSum + (sessionId & 0x0000FFFF);
        if (this.checkSum > this.USHRT_MAX) {
            this.checkSum = this.checkSum - this.USHRT_MAX;
        }

        this.checkSum = this.checkSum + (replyId & 0x000000FF);
        if (this.checkSum > this.USHRT_MAX) {
            this.checkSum = this.checkSum - this.USHRT_MAX;
        }

        if (this.arg != null) {
            byte[] argBytes = this.arg;
            for (int i = 0; i < argBytes.length; i = i + 2) {
                int valor = 0;
                if (i == (argBytes.length -1)) {
                    valor = ((0 & 0xFF) << 8 | (argBytes[i] & 0xFF)) & 0x0000FFFF;
                    this.checkSum = this.checkSum + valor;
                } else {
                    valor = ((argBytes[i + 1] & 0xFF) << 8 | (argBytes[i] & 0xFF)) & 0x0000FFFF;
                    this.checkSum = this.checkSum + valor;
                }
                if (this.checkSum > this.USHRT_MAX) {
                    this.checkSum = this.checkSum - this.USHRT_MAX;
                }
            }
        }

        this.checkSum = ~this.checkSum;
    }

    public byte[] getPayload() {
        byte[] payload = new byte[8 + this.len];

        //Header
        payload[0] = (byte) ((this.headerCommand & 0x000000FF));
        payload[1] = (byte) ((this.headerCommand & 0x0000FF00) >> 8);
        payload[2] = (byte) ((this.headerCommand & 0x00FF0000) >> 16);
        payload[3] = (byte) ((this.headerCommand & 0xFF000000) >> 24);
        
        //Len Message
        payload[4] = (byte) ((this.len & 0x000000FF));
        payload[5] = (byte) ((this.len & 0x0000FF00) >> 8);
        payload[6] = (byte) ((this.len & 0x00FF0000) >> 16);
        payload[7] = (byte) ((this.len & 0xFF000000) >> 24);
        
        //Command
        payload[8] = (byte) ((this.command & 0x000000FF));
        payload[9] = (byte) ((this.command & 0x0000FF00) >> 8);
        
        //CheckSum
        payload[10] = (byte) ((this.checkSum & 0x000000FF));
        payload[11] = (byte) ((this.checkSum & 0x0000FF00) >> 8);
        
        //SessionId
        payload[12] = (byte) ((this.sessionId & 0x000000FF));
        payload[13] = (byte) ((this.sessionId & 0x0000FF00) >> 8);
        
        //ReplyId
        payload[14] = (byte) ((this.replyId & 0x000000FF));
        payload[15] = (byte) ((this.replyId & 0x0000FF00) >> 8);
        
        //Arg
        if (this.arg != null) {
            byte[] argBytes = this.arg;
            for (int i =0; i < argBytes.length; i++) {
                payload[16 + i] = (byte) argBytes[i];
            }
        }
        
        return payload;
    }

    public void setSessionId(int sessionId) {
        this.sessionId = sessionId;
    }

    public int getSessionId() {
        return sessionId;
    }

    public void setReplyId(int replyId) {
        this.replyId = replyId;
    }

    public int getReplyId() {
        return replyId;
    }


}
