package cl.iodev.zklib4j;

public class Response {

    private int header = 0;
    private int length = 0;
    private int result = 0;
    private int checkSum = 0;
    private int sessionId = 0;
    private int replyId = 0;
    private byte[] argument;
    
    public Response(byte[] data) {
        super();
        
        this.header = ((data[3] << 24) & 0xFF000000) | 
                      ((data[2] << 16) & 0x00FF0000) | 
                      ((data[1] << 8) & 0x0000FF00) | 
                      (data[0] & 0x000000FF);
        
        this.length = ((data[7] << 24) & 0xFF000000) | 
                      ((data[6] << 16) & 0x00FF0000) | 
                      ((data[5] << 8) & 0x0000FF00) | 
                      (data[4] & 0x000000FF);
        
        this.result = ((data[9] << 8) & 0x0000FF00) | 
                      (data[8] & 0x000000FF);

        
        this.checkSum = ((data[11] << 8) & 0x0000FF00) | 
                      (data[10] & 0x000000FF);
        
        this.sessionId = ((data[13] << 8) & 0x0000FF00) | 
                      (data[12] & 0x000000FF);
        
        this.replyId = ((data[15] << 8) & 0x0000FF00) | 
                      (data[14] & 0x000000FF);
        
        if (data.length > 16) {
            argument = new byte[data.length - 16];
            for (int i = 16; i < data.length; i++) {
                argument[i - 16] = data[i];
            }
        }
    }


    public void setResult(int result) {
        this.result = result;
    }

    public int getResult() {
        return result;
    }

    public void setSessionId(int sessionId) {
        this.sessionId = sessionId;
    }

    public int getSessionId() {
        return sessionId;
    }

    public void setReplyId(int replyId) {
        this.replyId = replyId;
    }

    public int getReplyId() {
        return replyId;
    }

    public void setArgument(byte[] argument) {
        this.argument = argument;
    }

    public byte[] getArgument() {
        return argument;
    }

}
